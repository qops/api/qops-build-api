########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from qops_desktop import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Build(Base):
    __tablename__ = 'build'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    version = db.Column(db.String)
    kind_id = db.Column(db.Integer)
    stability_id = db.Column(db.Integer)
    build_start = db.Column(db.DateTime)
    build_finish = db.Column(db.DateTime)
    internal_code_name = db.Column(db.String)
    external_code_name = db.Column(db.String)

    def get_next_identifier():
        return 'BLD00001'


class ProductBuilds(Base):
    __tablename__ = 'product_builds'
    
    build_id = db.Column(db.Integer)
    product_id = db.Column(db.Integer)

    def BuildCount():
        count = db.query(func.count(ProductBuilds.Builds.id).filter(ProductBuilds.build_id == build_id).scalar())


class ProjectBuilds(Base):
    __tablename__ = 'project_builds'
    
    build_id = db.Column(db.Integer)
    project_id = db.Column(db.Integer)

    def BuildCount():
        count = db.query(func.count(ProjectBuilds.Builds.id).filter(ProjectBuilds.build_id == build_id).scalar())


class BuildKind(Base):
    __tablename__ = 'build_kind'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)


class BuildStability(Base):
    __tablename__ = 'build_stability'
    
    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)

