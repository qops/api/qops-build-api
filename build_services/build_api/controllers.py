########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

import logging

# Import models
from .models import Build
from .models import ProductBuilds
from .models import ProjectBuilds
from .models import BuildKind
from .models import BuildStability

from .forms import BuildProfileForm

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_build = Blueprint('build', __name__, url_prefix='/build')


@mod_build.context_processor
def store():
    store_dict = {'serviceName': 'Build',
                  'serviceDashboardUrl': url_for('build.dashboard'),
                  'serviceBrowseUrl': url_for('build.browse'),
                  'serviceNewUrl': url_for('build.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_build.route('/', methods=['GET'])
def build():
    return render_template('build/build_dashboard.html')


@mod_build.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('build/build_dashboard.html')


@mod_build.route('/browse', methods=['GET'])
def browse():
    builds = Build.query.with_entities(Build.id, Build.identifier, Build.name, Build.version, BuildKind.name.label('kind_id'), BuildStability.name.label(
        'stability_id')).join(BuildKind, Build.kind_id == BuildKind.id).join(BuildStability, Build.stability_id == BuildStability.id).all()
    return render_template('build/build_browse.html', builds=builds)


@mod_build.route('/new', methods=['GET', 'POST'])
def new():
    build = Build()
    form = BuildProfileForm(request.form)
    form.kind_id.choices = [(k.id, k.name) for k in BuildKind.query.all()]
    form.stability_id.choices = [(s.id, s.name)
                                 for s in BuildStability.query.all()]
    if request.method == 'POST':
        logger = logging.getLogger('qops')
        logger.debug('IN BUILD.NEW(POST)')
        logger.debug('Build Identifier: {0}'.format(form.identifier.data))
        logger.debug('Build Name      : {0}'.format(form.name.data))
        logger.debug('Build Version   : {0}'.format(form.version.data))
        logger.debug('Build Kind      : {0}'.format(form.kind_id.data))
        logger.debug('Build Stability : {0}'.format(form.stability_id.data))
        form.populate_obj(build)
        build.identifier = Build.get_next_identifier()
        logger.debug(build.kind_id)
        db.session.add(build)
        db.session.commit()
        return redirect(url_for('build.browse'))
    return render_template('build/build_new.html', build=build, form=form)


@mod_build.route('/profile', methods=['GET', 'POST'])
@mod_build.route('/profile/<int:build_id>', methods=['GET', 'POST'])
def profile(build_id=None):
    build = Build.query.get(build_id)
    form = BuildProfileForm(obj=build)
    form.kind_id.choices = [(k.id, k.name) for k in BuildKind.query.all()]
    form.stability_id.choices = [(s.id, s.name)
                                 for s in BuildStability.query.all()]
    if request.method == 'POST':
        logger = logging.getLogger('qops')
        logger.debug('IN BUILD.PROFILE(POST)')
        logger.debug('Build Identifier: {0}'.format(form.identifier.data))
        logger.debug('Build Name      : {0}'.format(form.name.data))
        logger.debug('Build Version   : {0}'.format(form.version.data))
        logger.debug('Build Kind      : {0}'.format(form.kind_id.data))
        logger.debug('Build Stability : {0}'.format(form.stability_id.data))
        form = BuildProfileForm(request.form)
        form.populate_obj(build)
        db.session.add(build)
        db.session.commit()
        return redirect(url_for('build.browse'))
    return render_template('build/build_profile.html', build=build, form=form)


@mod_build.route('/view', methods=['GET', 'POST'])
@mod_build.route('/view/<int:build_id>', methods=['GET', 'POST'])
def build_view(build_id=None):
    #build = Build.query.get(build_id)
    form = BuildProfileForm(obj=build)
    form.kind_id.choices = [(k.id, k.name) for k in BuildKind.query.all()]
    form.stability_id.choices = [(s.id, s.name)
                                 for s in BuildStability.query.all()]
    if request.method == 'POST':
        form = BuildProfileForm(request.form)
        form.populate_obj(build)
        db.session.add(build)
        db.session.commit()
        return redirect(url_for('build.browse'))
    return render_template('build/build_view.html', build=build,
                           form=form)


@mod_build.route('/profile/dashboard', methods=['GET'])
@mod_build.route('/profile/<int:build_id>/dashboard', methods=['GET'])
def build_dashboard(build_id=None):
    if build_id:
        build = Build.query.get(build_id)
    else:
        build = None
    return render_template('build/build_single_dashboard.html', build=build)


@mod_build.route('/profile/notes', methods=['GET'])
@mod_build.route('/profile/<int:build_id>/notes', methods=['GET'])
def build_notes(build_id=None):
    if build_id:
        build = Build.query.get(build_id)
    else:
        build_id = None
    return render_template('build/build_single_notes.html', build=build)


@mod_build.route('/profile/issues', methods=['GET', 'POST'])
@mod_build.route('/profile/<int:build_id>/issues', methods=['GET'])
def build_issues(build_id=None):
    if build_id:
        build = Build.query.get(build_id)
    else:
        build = None
    return render_template('build/build_single_issues.html', build=build)
